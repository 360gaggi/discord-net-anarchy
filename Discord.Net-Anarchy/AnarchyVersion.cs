namespace Discord.Net_Anarchy
{
    public class AnarchyVersion
    {
        public static string GetVersion() => "Discord.Net-Anarchy v1.9.1 by 360gaggi";

        public static string GetDiscordVersion() => "v3.16.1";
    }
}
