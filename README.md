# This is basically Discord .Net but I added some more features (back) like user login, some stuff in Group and DM-Channels, etc. All the changes that i made are listed below


## user

- SocketSelfUser.cs:59 -> added upload limit method
- RestSelfUser.cs:28 -> added upload limit method
- ISelfUser.cs:61 -> added upload limit method
- UserHelper.cs:113 -> added upload limit method


## guild

- SocketGuild.cs:687 -> Made User nullable in GetBansAsync
- SocketGuild.cs:1607 -> removed check if there are stickers in GetStickersAsync()
- RestGuild.cs:367 -> Made User nullable in GetBansAsync
- GuildHelper.cs:447 -> Changed max of 5 to 20 in forum tags
- GuildHelper.cs:1393 -> fixed nullreference in Welcomescreen get
- GuildHelper.cs:1423 -> fixed nullreference in Welcomescreen get


## text channel

- SocketTextChannel.cs:138 -> Added GetArchivedThreadsAsync method
- SocketTextChannel.cs:145 -> Added GetAllThreadChannels method which gets both active an archived thread channels
- SocketTextChannel.cs:408 -> Implemented GetArchivedThreadsAsync method
- RestTextChannel.cs:296 -> Added GetArchivedThreadsAsync method
- RestTextChannel.cs:343 -> Implemented GetArchivedThreadsAsync method
- ITextChannel.cs:160 -> Added GetArchivedThreadsAsync method
- ChannelHelper.cs:413 -> fixed user problem with upload (aka added isbot check)


## thread channel

- ThreadHelper.cs:84 -> added GetArchivedThreads method


## forum channel

- SocketForumChannel.cs:17 -> added DebuggerDisplay attribute
- SocketForumChannel.cs:100 -> made CategoryId nullable
- SocketForumChannel.cs:150 -> Added GetArchivedThreadsAsync method
- SocketForumChannel.cs:170 -> Added GetAllPostsAsync method which tries to get both active and archived posts from Forum
- SocketForumChannel.cs:176 -> added DebuggerDisplay
- SocketForumChannel.cs:178 -> added clone
- SocketForumChannel.cs:211 -> Implemented GetArchivedThreadsAsync method
- RestForumChannel.cs:16 -> added DebuggerDisplay attribute
- RestForumChannel.cs:94 -> made CategoryId nullable
- RestForumChannel.cs:148 -> Implemented GetArchivedThreadsAsync method
- RestForumChannel.cs:167 -> Added GetAllPostsAsync method which tries to get both active and archived posts from Forum
- RestForumChannel.cs:173 -> added DebuggerDisplay
- RestForumChannel.cs:190 -> Added GetArchivedThreadsAsync method
- IForumChannel.cs:247 -> added GetArchivedThreadsAsync method
- ForumTag.cs:45 -> Added .Build() method for easier access


## group channel

- SocketGroupChannel.cs:27 -> renamed iconId to ownerId
- SocketGroupChannel.cs:30 -> Created public IconId prop 
- SocketGroupChannel.cs:44 -> Added Owner Property
- SocketGroupChannel.cs:49 -> fixed Users get
- SocketGroupChannel.cs:57 -> fixed Recipients get
- SocketGroupChannel.cs:79 -> fixed IconId
- SocketGroupChannel.cs:81 -> assigned ownerId field
- SocketGroupChannel.cs:106 -> Added GetIconUrl
- SocketGroupChannel.cs:341 -> Added implementations of Users
- SocketGroupChannel.cs:343 -> Added implementations of CurrentUser
- RestGroupChannel.cs:20 -> renamed iconId to ownerId
- RestGroupChannel.cs:23 -> Created public IconId prop 
- RestGroupChannel.cs:30 -> Added Owner Property
- RestGroupChannel.cs:32 -> fixed Users get
- RestGroupChannel.cs:37 -> fixed Recipients get
- RestGroupChannel.cs:54 -> fixed IconId
- RestGroupChannel.cs:57 -> assigned ownerId field
- RestGroupChannel.cs:88 -> Added GetIconUrl
- IGroupChannel.cs:22 -> added Owner
- IGroupChannel.cs:28 -> added GetIconUrl()


## dm channel

- SocketDMChannel.cs:260 -> Added implementations of Users
- SocketDMChannel.cs:262 -> Added implementations of CurrentUser
- ISocketPrivateChannel.cs:12 -> Added Users
- ISocketPrivateChannel.cs:14 -> Added CurrentUser


## discord client

- DiscordSocketClient.cs:253 -> added validation to check if its a user or bot token
- DiscordSocketClient.EventHandling.cs:107 -> data.Application is null for users
- DiscordShardedClient.cs:2 -> added using Discord.Net.Queue
- DiscordShardedClient.cs:298 -> fixed null exception if sticker is from an unknown guild
- DiscordShardedClient.cs:434 -> removed client.SentRequest (added further down)
- DiscordShardedClient.cs:533 -> added before and after request sent events (if the BeforeRequestSent func is set it will override the ones configured at the base idk if thats correct tho, if not pls text me and tell me how to solve that)
- DiscordShardedClient.cs:648 -> added global before and after request sent events for the sharded client to receive all of them from the shards
- DiscordWebhookClient.cs:9 -> added using Discord.Net.Queue
- DiscordWebhookClient.cs:30 -> added before and after request sent events + a func to potentially await the request thats being sent
- DiscordWebhookClient.cs:109 -> implemented BeforeSentRequest event
- DiscordWebhookClient.cs:117 -> implemented AfterSentRequest event and fixed params
- BaseDiscordClient.cs:2 -> added using Discord.Net.Queue
- BaseDiscordClient.cs:24 -> added before and after request sent events + a func to potentially await the request thats being sent
- BaseDiscordClient.cs:71 -> implemented BeforeSentRequest event
- BaseDiscordClient.cs:79 -> implemented AfterSentRequest event and fixed params
- DiscordRestApiClient.cs:33 -> adjusted the sentRequestEvent params
- DiscordRestApiClient.cs:36 -> added BeforeRequestSent event
- DiscordRestApiClient.cs:51 -> add setter for AuthTokenType and Call SetBaseUrl() in there
- DiscordRestApiClient.cs:101 -> Add User and webhook to option and just return token without anything else
- DiscordRestApiClient.cs:309 -> fire the BeforeRequestSent event
- DiscordRestApiClient.cs:316 -> adjusted the sentRequestEvent params


## messages

- IMessage.cs:218 -> moved Poll property from IUserMessage.cs:39 to there cus idk why its only in IUserMessage tbh
- IUserMessage.cs:44 -> removed poll property
- RestMessage.cs: 94 -> added Poll property (also changed it to protected set)
- RestUserMessage.cs: 55 -> removed poll property
- SocketMessage.cs: 82 -> added Poll property (also changed it to protected set)
- SocketUserMessage.cs: 56 -> removed poll property
- ComponentType.cs:51 -> added type activity 
- MessageComponentConverter.cd:43 -> added type activity



## token

- TokenType.cs:23 -> Added user token type
- TokenUtils.cs:160 -> added user token to switch to skip validation

## miscellaneous

- SocketCustomSticker.cs:50 -> added update override so i can actually have the author of the sticker
- CDN.cs:260 -> added gif option in sticker
- CDN.cs:268 -> fixed nullreference exception with imageId
- FileAttachment.cs:66 -> added new constrcutor that instead of taking a stream takes a byte[] of data
- SocketInteractionContext.cs:16,49,92 -> Replaced DiscordSocketClient with BaseSocketClient
- RequestQueue.cs:73 -> added a preemtive check if the canceltoken has been cancelled
- AsyncEvent.cs:75 -> added 2 new invoke methods
- InteractionService.cs:781 -> added SearchModalCommand method

## Pull requests

- none


## own additions

- DiscordRestClient.cs:338 -> added RefreshUrlAsync and RefreshUrlsAsync method
- DiscordRestApiClient.cs:2595 -> added RefreshUrlsAsync method
- RefreshedUrl.cs -> class added in Discord.Net.Rest/API/Common for the things above
- fixed various ToDictionary() with "ToDictionary(i => i.Key, j => j.Value)" cus compiler gave error


# helpers

## kyle

- Subscribe method to get all events for a guild that may not fire events such as messages
- Usage:
- DiscordSocketClient client = new DiscordSocketClient();
- await client.SubscribeToGuildEvents(guild-id);
- Changes:
- added ClientExtensions.cs class in Discord.WebSocket
- GatewayOpCode.cs:32 -> added GuildSubscription enum




### Cant wait for discord .Net to finally make a new release of feature x? Add them here yourself and make a PR!
