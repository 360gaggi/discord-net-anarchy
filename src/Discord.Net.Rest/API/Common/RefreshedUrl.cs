using Newtonsoft.Json;

namespace Discord.API
{
    public class RefreshedUrl
    {
        [JsonProperty("refreshed_urls")]
        public RefreshedUrlInfo[] RefreshedUrls { get; set; }
    }

    public class RefreshedUrlInfo
    {
        [JsonProperty("original")]
        public string OriginalUrl { get; set; }

        [JsonProperty("refreshed")]
        public string RefreshedUrl { get; set; }
    }
}
