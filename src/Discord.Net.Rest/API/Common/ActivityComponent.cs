using Newtonsoft.Json;
using System;
using System.Net.Mime;

namespace Discord.API
{
    internal class ActivityComponent : IMessageComponent
    {
        [JsonProperty("type")]
        public ComponentType Type { get; set; }

        [JsonProperty("id")]
        public ulong Id { get; set; }

        [JsonProperty("custom_id")]
        public string CustomId { get; set; }

        [JsonProperty("content_inventory_entry")]
        public InventoryEntry Entry { get; set; }

        public ActivityComponent() { }

        public ActivityComponent(Discord.ActivityComponent component)
        {
            Type = component.Type;
            Style = component.Style;
            CustomId = component.CustomId;
            Label = component.Label;
            Placeholder = component.Placeholder;
            MinLength = component.MinLength ?? Optional<int>.Unspecified;
            MaxLength = component.MaxLength ?? Optional<int>.Unspecified;
            Required = component.Required ?? Optional<bool>.Unspecified;
            Value = component.Value ?? Optional<string>.Unspecified;
        }
    }

    internal class InventoryEntry
    {
        [JsonProperty("id")]
        public ulong Id{ get; set; }

        [JsonProperty("author_id")]
        public ulong AuthorId { get; set; }

        [JsonProperty("author_type")]
        public Discord.AuthorType AuthorType { get; set; }

        [JsonProperty("content_type")]
        public Discord.ContentType ContentType { get; set; }

        [JsonProperty("traits")]
        public InventoryTraits[] Traits { get; set; }

        [JsonProperty("extra")]
        public InventoryExtra Extra { get; set; }

        [JsonProperty("participants")]
        public ulong[] Participants { get; set; }

        [JsonProperty("expires_at")]
        public DateTime ExpiresAt { get; set; }

        [JsonProperty("ended_at")]
        public DateTime EndedAt { get; set; }

        [JsonProperty("started_at")]
        public DateTime StartedAt { get; set; }


        public InventoryEntry() { }
    }
}

/*
playing rn, x streak
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"1308020399270793237",
         "author_id":"300532463648899073",
         "author_type":1,
         "content_type":1,
         "traits":[
            {
               "type":2,
               "duration_seconds":0
            },
            {
               "type":8,
               "streak_count_days":119
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"Honkai: Star Rail",
            "application_id":"1121201675240210523",
            "platform":0
         },
         "participants":[
            "300532463648899073"
         ],
         "expires_at":"2024-11-18T11:21:05.125615+00:00",
         "ended_at":"2024-11-18T10:46:05.030000+00:00",
         "started_at":"2024-11-18T10:46:05.030000+00:00"
      }
   }
},


played x days ago, x streak
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"1307825020478816296",
         "author_id":"300532463648899073",
         "author_type":1,
         "content_type":1,
         "traits":[
            {
               "type":2,
               "duration_seconds":8006
            },
            {
               "type":8,
               "streak_count_days":10
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"League of Legends",
            "application_id":"356869127241072640",
            "platform":0
         },
         "participants":[
            "300532463648899073"
         ],
         "ended_at":"2024-11-17T21:49:43.096000+00:00",
         "started_at":"2024-11-17T19:36:17.096000+00:00"
      }
   }
}


played x days ago, new release, new player, trending
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"1307494286245236870",
         "author_id":"306501331135234048",
         "author_type":1,
         "content_type":1,
         "traits":[
            {
               "type":2,
               "duration_seconds":5808
            },
            {
               "type":1,
               "first_time":true
            },
            {
               "type":8,
               "streak_count_days":2
            },
            {
               "type":9,
               "trending":1
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"Black Ops 6",
            "application_id":"1306357637893587014",
            "platform":0
         },
         "participants":[
            "306501331135234048"
         ],
         "ended_at":"2024-11-16T23:55:29.910000+00:00",
         "started_at":"2024-11-16T22:18:41.910000+00:00"
      }
   }
}


played x days ago, trending, returning
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"1300981465932103721",
         "author_id":"352120233974169600",
         "author_type":1,
         "content_type":1,
         "traits":[
            {
               "type":2,
               "duration_seconds":230
            },
            {
               "type":5,
               "resurrected_last_played":"2024-02-15T15:26:59.006000+00:00"
            },
            {
               "type":9,
               "trending":1
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"Don't Starve Together",
            "application_id":"359508004078616586",
            "platform":0
         },
         "participants":[
            "352120233974169600"
         ],
         "ended_at":"2024-10-30T00:35:52.545000+00:00",
         "started_at":"2024-10-30T00:32:02.545000+00:00"
      }
   }
}


most played, x hours this week
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"741282585",
         "author_id":"280107663503917056",
         "author_type":1,
         "content_type":3,
         "traits":[
            {
               "type":4,
               "range":1
            },
            {
               "type":2,
               "duration_seconds":191757
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"League of Legends",
            "application_id":"356869127241072640",
            "platform":0
         },
         "original_id":"1307825171922554901",
         "participants":[
            "280107663503917056"
         ]
      }
   }
}


played x weeks ago, new player
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"1304171671321837660",
         "author_id":"300260910981971972",
         "author_type":1,
         "content_type":1,
         "traits":[
            {
               "type":2,
               "duration_seconds":3783
            },
            {
               "type":1,
               "first_time":true
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"In Silence",
            "application_id":"1124351773067456552",
            "platform":0
         },
         "participants":[
            "300260910981971972"
         ],
         "ended_at":"2024-11-07T19:52:36.781000+00:00",
         "started_at":"2024-11-07T18:49:33.781000+00:00"
      }
   }
}


played x weeks ago, marathon
{
   "components":{
      "type":16,
      "id":1,
      "content_inventory_entry":{
         "id":"1302475852407246958",
         "author_id":"217792113729863680",
         "author_type":1,
         "content_type":1,
         "traits":[
            {
               "type":2,
               "duration_seconds":14515
            },
            {
               "type":6,
               "marathon":true
            },
            {
               "type":8,
               "streak_count_days":2
            }
         ],
         "extra":{
            "type":"played_game_extra",
            "game_name":"Metaphor: ReFantazio",
            "application_id":"1294162617006297148",
            "platform":0
         },
         "participants":[
            "217792113729863680"
         ],
         "ended_at":"2024-11-03T03:34:02.045000+00:00",
         "started_at":"2024-11-02T23:32:07.045000+00:00"
      }
   }
}


Author type:
1 = idk
2 = ???
3 = ???

Content type:
1 = idk
2 = ???
3 = Idk (maybe most played)


Trait types:
1 = new player
2 = playing rn (if seconds = 0) / last playtime (+ seconds)
3 = ???
4 = most played
5 = returning
6 = marathon
7 = ???
8 = streak count
9 = trending


time is UTC






*/
