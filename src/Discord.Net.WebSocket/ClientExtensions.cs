using System.Collections.Generic;
using System.Threading.Tasks;
using Discord.API.Gateway;
using Newtonsoft.Json;

namespace Discord.WebSocket;

public static class ClientExtensions
{
    public static Task SubscribeToGuildEvents(this BaseSocketClient client, ulong guildId)
    {
        return client.ApiClient.SendGatewayAsync(GatewayOpCode.GuildSubscription, new GuildSub
        {
            Typing = true,
            Activities = true,
            Threads = true,
            GuildId = guildId
        }, RequestOptions.CreateOrClone(null));
    }

    public class GuildSub
    {
        [JsonProperty("guild_id")]
        internal ulong GuildId { get; set; }

        [JsonProperty("typing")]
        public bool Typing { get; set; }

        [JsonProperty("threads")]
        public bool Threads { get; set; }

        [JsonProperty("activities")]
        public bool Activities { get; set; }

        [JsonProperty("members")]
        public List<ulong> Members { get; set; } = new List<ulong>();

        [JsonProperty("channels")]
        public Dictionary<ulong, int[][]> Channels { get; set; } = new Dictionary<ulong, int[][]>();

        [JsonProperty("thread_member_lists")]
        public List<object> ThreadMemberLists { get; set; } = new List<object>();
    }
}
